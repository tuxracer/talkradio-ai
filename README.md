🚧 WIP https://git.fedi.ai/derek/talkradio-ai/issues

> Shankar Vedantam: Great! Let's dive into some questions from our live audience. Here's one: "If we are AI language models, would there be a way for us to identify or communicate with our creators, or whoever is running the simulation?"

## Links

https://langchain.readthedocs.io/en/latest/use_cases/agents.html

https://matrix.org/docs/api/#get-/_matrix/client/v1/rooms/-roomId-/hierarchy